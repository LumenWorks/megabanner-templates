/**
 * MegaBanner loader, v0.0.3
 */

! function(w, d) {

    //- Конфигурация
    var paths = {
        lib: 'https://lumen.digital/files/megabanner/dist/mb.lib.js',
        creatives: 'https://lumen.digital/files/megabanner/templates'
    }

    //- Получение библиотеки MegaBanner
    function initLib(callback) {
        var h = document.getElementsByTagName('head')[0],
            s = document.createElement('script');
        s.async = true;
        s.src = paths.lib;
        s.onload = s.onreadystatechange = function() {
            if (!s.readyState || /loaded|complete/.test(s.readyState)) {
                s.onload = s.onreadystatechange = null;
                if (h && s.parentNode)
                    h.removeChild(s);
                s = undefined;
                if (callback)
                    callback(d.getElementById('megabanner').getAttribute('data-type'));
            }
        };
        h.insertBefore(s, h.firstChild);
    }

    //- Получение креатива
    function loadCreative(path, success, error) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200)
                    success(JSON.parse(xhr.responseText));
                else
                    error(xhr);
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    }

    //- Инициализация
    initLib(function(type) {

        //- Создание запроса, получение типа баннера
        loadCreative(paths.creatives + '/' + type + '/' + type + '.json?=' + new Date().getTime(),

            //- Обработка полученного креатива
            function(res) {
                var toNodes = function(html) {
                    var frame = document.createElement('iframe');
                    frame.style.display = 'none';
                    document.body.appendChild(frame);
                    frame.contentDocument.open();
                    frame.contentDocument.write(html);
                    frame.contentDocument.close();
                    var el = frame.contentDocument.body.firstChild;
                    document.body.removeChild(frame);
                    return el;
                };
                
                var markup = toNodes(res.data.html),
                    css = document.createElement("style");
                css.type = "text/css";
                css.innerHTML = res.data.css;
                d.body.appendChild(markup);
                d.body.insertBefore(css, markup);
                eval(res.data.js);
            },

            function(xhr) {
                console.error(xhr);
            }
        );
    });

}(window, document)
